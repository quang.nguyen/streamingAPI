package com.tmovie.streaming.form;

import java.util.Optional;

import com.tmovie.streaming.Filmfeed;
import com.tmovie.streaming.Filmfeed.Data;

public class FilmForm {
	public static class TVShow {
		public String episode;
		public String season;
		public String premiered;
	}
	public String title;
	public String imdb;
	public Integer year;
	public TVShow show;
	public Integer tracking = 0;
	
	public static Data convertToFilmRequest(FilmForm form) {
		Data.Builder builder = Data.newBuilder();
		Optional.ofNullable(form.title)
		.ifPresent(value -> builder.setTitle(value));
		
		Optional.ofNullable(form.imdb)
		.ifPresent(value -> builder.setImdb(value));
		
		Optional.ofNullable(form.year)
		.ifPresent(value -> builder.setYear(value));
		
		Optional.ofNullable(form.show)
		.ifPresent(value -> {
			Filmfeed.TVShow.Builder tvShowBuilder = Filmfeed.TVShow.newBuilder();
			Optional.ofNullable(value.episode)
			.ifPresent(episode -> tvShowBuilder.setEpisode(episode));
			
			Optional.ofNullable(value.season)
			.ifPresent(season -> tvShowBuilder.setSeason(season));
			
			Optional.ofNullable(value.premiered)
			.ifPresent(premiered -> tvShowBuilder.setPremiered(premiered));
			
			builder.setShow(tvShowBuilder);
		});
		
		return builder.build();
	}
}

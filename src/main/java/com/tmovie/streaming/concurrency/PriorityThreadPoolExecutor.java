package com.tmovie.streaming.concurrency;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class PriorityThreadPoolExecutor extends ThreadPoolExecutor {

	
	public PriorityThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
	}

	public PriorityThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
	}

	public PriorityThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
	}

	public PriorityThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit,
			BlockingQueue<Runnable> workQueue) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
	}

	public <T> ComparableFutureTask<T> submit(Callable<T> task, BasePriority priority) {
		if (task == null) throw new NullPointerException();
		ComparableFutureTask<T> ftask = newTaskFor(task, priority);
        execute(ftask);
        return ftask;
	}
	
	public ComparableFutureTask<?> submit(Runnable task, BasePriority priority) {
		if (task == null) throw new NullPointerException();
		ComparableFutureTask<Void> ftask = newTaskFor(task, null, priority);
		execute(ftask);
		return ftask;
	}
	
	public <T> ComparableFutureTask<T> submit(Runnable task, T result, BasePriority priority) {
		 if (task == null) throw new NullPointerException();
		 ComparableFutureTask<T> ftask = newTaskFor(task, result, priority);
		 execute(ftask);
		 return ftask;
	}
	
	protected <T> ComparableFutureTask<T> newTaskFor(Callable<T> callable, BasePriority priority) {
		return new ComparableFutureTask<T>(callable, priority);
	}
	
	protected <T> ComparableFutureTask<T> newTaskFor(Runnable runnable, T value, BasePriority priority) {
		return new ComparableFutureTask<T>(runnable, value, priority);
	}
	
}

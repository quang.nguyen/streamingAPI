package com.tmovie.streaming.configuration;


import java.util.Collections;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.tmovie.streaming.concurrency.PriorityThreadPoolExecutor;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
	
	@Bean
	public ManagedChannel channel() {
		return ManagedChannelBuilder.forAddress("localhost", 9090)
				.usePlaintext(true)
				.build();
	}
	
	@Bean
	public PriorityThreadPoolExecutor priorityTPE() {
		PriorityBlockingQueue<Runnable> pbq =
		        new PriorityBlockingQueue<Runnable>(1024, Collections.reverseOrder());
		PriorityThreadPoolExecutor pool = 
				new PriorityThreadPoolExecutor(1, 1, 1000L, TimeUnit.MILLISECONDS, pbq);
		return pool;
	}
	
}

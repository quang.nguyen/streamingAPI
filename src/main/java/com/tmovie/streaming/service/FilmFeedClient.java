package com.tmovie.streaming.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.util.concurrent.ListenableFuture;
import com.tmovie.streaming.StreamingGrpc;
import com.tmovie.streaming.Filmfeed.Data;
import com.tmovie.streaming.Filmfeed.Result;
import com.tmovie.streaming.StreamingGrpc.StreamingBlockingStub;
import com.tmovie.streaming.StreamingGrpc.StreamingFutureStub;
import com.tmovie.streaming.StreamingGrpc.StreamingStub;

import io.grpc.ManagedChannel;
import io.grpc.stub.StreamObserver;

@Service
public class FilmFeedClient {

	private StreamingFutureStub futureStub;
	private StreamingStub asyncStub;
	private StreamingBlockingStub blockingStub;

	@Autowired
	public FilmFeedClient(ManagedChannel channel) {
		this.futureStub = StreamingGrpc.newFutureStub(channel);
		this.asyncStub = StreamingGrpc.newStub(channel);
		this.blockingStub = StreamingGrpc.newBlockingStub(channel);
	}
	
	public ListenableFuture<Result> getFutureStreaming(Data data) {
		return this.futureStub.getStreaming(data);
	}
	
	public Result getBlockingStreaming(Data data) {
		return this.blockingStub.getStreaming(data);
	}
	
	public void getAsyncStreaming(Data data, StreamObserver<Result> responseObserver) {
		this.asyncStub.getStreaming(data, responseObserver);
	}
}

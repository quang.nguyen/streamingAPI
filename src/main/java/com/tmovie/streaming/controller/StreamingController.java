package com.tmovie.streaming.controller;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tmovie.streaming.Filmfeed.Data;
import com.tmovie.streaming.Filmfeed.Result;
import com.tmovie.streaming.concurrency.BasePriority;
import com.tmovie.streaming.concurrency.PriorityThreadPoolExecutor;
import com.tmovie.streaming.form.FilmForm;
import com.tmovie.streaming.form.FilmForm.TVShow;
import com.tmovie.streaming.form.FilmRequestType;
import com.tmovie.streaming.response.FilmResponse;
import com.tmovie.streaming.service.FilmFeedClient;

@RestController
@EnableAsync
public class StreamingController {
	private static Logger log = LoggerFactory.getLogger(StreamingController.class);
	private PriorityThreadPoolExecutor pool;
	private FilmFeedClient feedClient;
	
	@Autowired
	public StreamingController(FilmFeedClient feedClient, PriorityThreadPoolExecutor pool) {
		this.feedClient = feedClient;
		this.pool = pool;
	}

	@PostMapping(value = "/blockingStreaming")
	public ResponseEntity<FilmResponse> blockingStream(@RequestBody FilmForm form){
		HttpStatus resStatus = HttpStatus.BAD_REQUEST;
		FilmResponse res = new FilmResponse();
		try {
			Data data = FilmForm.convertToFilmRequest(form);
			Result result = this.feedClient.getBlockingStreaming(data);
			res.link = result.getUrl();
			res.resultCode = 1;
			resStatus = HttpStatus.OK;
		} catch (Exception e) {
			res.resultCode = -1;
		} finally {
			res.tracking = form.tracking + 1;
		}
		return new ResponseEntity<FilmResponse>(res, resStatus);
	}
	
	@GetMapping(value = "/testPool")
	public ResponseEntity<String> testPool(){
		List<String> films = Arrays.asList("Logan", "Thor", "IronMan", "Nobita", "Ip man",
				"Memento", "Interstellar", "Shutter Island", "La la na", "Batman");
		int initialTracking = 0;
		for(int i=0; i<films.size(); i++) {
			String film = films.get(i);
			FilmForm form = new FilmForm();
			form.imdb = film + "imdb";
			form.title = film;
			form.year = 2017;
			form.tracking = initialTracking; 
			form.show = new TVShow();
			form.show.episode = film + "episode";
			form.show.premiered = film + "premiered";
			form.show.season = film + "season";
			Data data = FilmForm.convertToFilmRequest(form);
			this.pool.submit(() -> {
				FilmResponse res = new FilmResponse();
				try {
					Result result = this.feedClient.getBlockingStreaming(data);
					res.link = result.getUrl();
					res.resultCode = 1;
				} catch (Exception e) {
					res.resultCode = -1;
				} finally {
					res.tracking = form.tracking + 1;
				}
				log.debug("{} is crawled. {}", data.getTitle(), res.link);
				return res;
			}
			, new BasePriority(new Long(i)
					, (i%2) == 0 ? FilmRequestType.CLIENT : FilmRequestType.WEB_SERVER
					, form.tracking))
			.whenComplete((fRes, ex) -> {
				log.debug("{} is crawled.", fRes.link);
			});
			initialTracking = initialTracking + (i%2);
		}
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/asyncTest")
    public Future<FilmResponse> asyncTest(){
        int initialTracking = 0;
        String film = "Logan";
        FilmForm form = new FilmForm();
        form.imdb = film + "imdb";
        form.title = film;
        form.year = 2017;
        form.tracking = initialTracking; 
        form.show = new TVShow();
        form.show.episode = film + "episode";
        form.show.premiered = film + "premiered";
        form.show.season = film + "season";
        Data data = FilmForm.convertToFilmRequest(form);
        CompletableFuture<FilmResponse> filmFuture = this.pool.submit(() -> {
            FilmResponse res = new FilmResponse();
            try {
                Result result = this.feedClient.getBlockingStreaming(data);
                res.link = result.getUrl();
                res.resultCode = 1;
            } catch (Exception e) {
                res.resultCode = -1;
            } finally {
                res.tracking = form.tracking + 1;
            }
            log.debug("{} is crawled. {}", data.getTitle(), res.link);
            return res;
        }
        , new BasePriority(new Long(0)
                , FilmRequestType.CLIENT
                , form.tracking));
        return filmFuture;
    }
}

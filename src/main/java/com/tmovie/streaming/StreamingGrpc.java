package com.tmovie.streaming;

import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.4.0)",
    comments = "Source: filmfeed.proto")
public final class StreamingGrpc {

  private StreamingGrpc() {}

  public static final String SERVICE_NAME = "com.tmovie.streaming.Streaming";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static final io.grpc.MethodDescriptor<com.tmovie.streaming.Filmfeed.Data,
      com.tmovie.streaming.Filmfeed.Result> METHOD_GET_STREAMING =
      io.grpc.MethodDescriptor.<com.tmovie.streaming.Filmfeed.Data, com.tmovie.streaming.Filmfeed.Result>newBuilder()
          .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
          .setFullMethodName(generateFullMethodName(
              "com.tmovie.streaming.Streaming", "getStreaming"))
          .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.tmovie.streaming.Filmfeed.Data.getDefaultInstance()))
          .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
              com.tmovie.streaming.Filmfeed.Result.getDefaultInstance()))
          .build();

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static StreamingStub newStub(io.grpc.Channel channel) {
    return new StreamingStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static StreamingBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new StreamingBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static StreamingFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new StreamingFutureStub(channel);
  }

  /**
   */
  public static abstract class StreamingImplBase implements io.grpc.BindableService {

    /**
     */
    public void getStreaming(com.tmovie.streaming.Filmfeed.Data request,
        io.grpc.stub.StreamObserver<com.tmovie.streaming.Filmfeed.Result> responseObserver) {
      asyncUnimplementedUnaryCall(METHOD_GET_STREAMING, responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            METHOD_GET_STREAMING,
            asyncUnaryCall(
              new MethodHandlers<
                com.tmovie.streaming.Filmfeed.Data,
                com.tmovie.streaming.Filmfeed.Result>(
                  this, METHODID_GET_STREAMING)))
          .build();
    }
  }

  /**
   */
  public static final class StreamingStub extends io.grpc.stub.AbstractStub<StreamingStub> {
    private StreamingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StreamingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StreamingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StreamingStub(channel, callOptions);
    }

    /**
     */
    public void getStreaming(com.tmovie.streaming.Filmfeed.Data request,
        io.grpc.stub.StreamObserver<com.tmovie.streaming.Filmfeed.Result> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(METHOD_GET_STREAMING, getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class StreamingBlockingStub extends io.grpc.stub.AbstractStub<StreamingBlockingStub> {
    private StreamingBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StreamingBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StreamingBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StreamingBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.tmovie.streaming.Filmfeed.Result getStreaming(com.tmovie.streaming.Filmfeed.Data request) {
      return blockingUnaryCall(
          getChannel(), METHOD_GET_STREAMING, getCallOptions(), request);
    }
  }

  /**
   */
  public static final class StreamingFutureStub extends io.grpc.stub.AbstractStub<StreamingFutureStub> {
    private StreamingFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private StreamingFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected StreamingFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new StreamingFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.tmovie.streaming.Filmfeed.Result> getStreaming(
        com.tmovie.streaming.Filmfeed.Data request) {
      return futureUnaryCall(
          getChannel().newCall(METHOD_GET_STREAMING, getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_STREAMING = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final StreamingImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(StreamingImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_STREAMING:
          serviceImpl.getStreaming((com.tmovie.streaming.Filmfeed.Data) request,
              (io.grpc.stub.StreamObserver<com.tmovie.streaming.Filmfeed.Result>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static final class StreamingDescriptorSupplier implements io.grpc.protobuf.ProtoFileDescriptorSupplier {
    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.tmovie.streaming.Filmfeed.getDescriptor();
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (StreamingGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new StreamingDescriptorSupplier())
              .addMethod(METHOD_GET_STREAMING)
              .build();
        }
      }
    }
    return result;
  }
}

package com.tmovie.streaming.response;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FilmResponse {
	public int resultCode;
	public String link;
	public int tracking;
}
